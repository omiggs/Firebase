package com.vidroid.com.br.apps.firebaseapp;

import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity {

    FirebaseAuth usuario = FirebaseAuth.getInstance();
    TextInputEditText editUser, editPass;
    Button btnLogar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editUser = findViewById(R.id.editUsuario);
        editPass = findViewById(R.id.editPass);
        btnLogar = findViewById(R.id.btnLogar);

        btnLogar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editUser.getText().toString().isEmpty()){
                    editUser.setError("Digite algo");
                }else if(editPass.getText().toString().isEmpty()){
                    editPass.setError("Digite algo");
                }else{
                    String usuarioEmail = editUser.getText().toString();
                    String password = editPass.getText().toString();

                    usuario.signInWithEmailAndPassword(usuarioEmail, password)
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if(task.isSuccessful()){
                                        Log.i("LoginUser", "Sucesso");
                                        finish();
                                    }else{
                                        Log.i("LoginUser", "Fracasso");
                                        Toast.makeText(
                                                getApplicationContext(),
                                                "E-Mail ou senha incorretos",
                                                Toast.LENGTH_LONG)
                                                .show();
                                    }
                                }
                            });
                }
            }
        });

    }
}
