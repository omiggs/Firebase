package com.vidroid.com.br.apps.firebaseapp;

import android.content.Intent;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.io.FileReader;

public class MainActivity extends AppCompatActivity {

    private DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
    private FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //Deslogar usuário
        Button button = findViewById(R.id.button2);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               firebaseAuth.signOut();
            }
        });


        //Logar uusário
        Button buttonLog = findViewById(R.id.button3);
        buttonLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(firebaseAuth.getCurrentUser() != null){
                    Toast.makeText(
                            getApplicationContext(),
                            "Usuário já logado",
                            Toast.LENGTH_LONG
                    ).show();
                }
                else{
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                }

            }
        });


        DatabaseReference usuariosDB = databaseReference.child("usuarios");

        //DatabaseReference usuariosPesquisa = usuariosDB.child("-L5_aAeZctzHu1wQdgZi");
        //Query usuariosPesquisa = usuariosDB.orderByKey().limitToFirst(3);
        //Query usuariosPesquisa = usuariosDB.orderByKey().limitToLast(3);
        //Query usuariosPesquisa = usuariosDB.orderByChild("nome").equalTo("Otavio");

        //Maior ou igual
        //Query usuariosPesquisa = usuariosDB.orderByChild("idade").startAt(15);

        //Menor ou igual
        //Query usuariosPesquisa = usuariosDB.orderByChild("idade").endAt(26);

        //Entre dois valores
        Query usuariosPesquisa = usuariosDB.orderByChild("idade").startAt(15).endAt(26);

        usuariosPesquisa.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //Usuarios usuarioPesquisado = dataSnapshot.getValue(Usuarios.class);
                Log.i("Dados usuário: ", dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        /*
        Usuarios usuarios = new Usuarios();
        usuarios.setNome("Otavio");
        usuarios.setSobrenome("Miguel");
        usuarios.setIdade(20);

        usuariosDB.push().setValue(usuarios);
         */


        /*Verifica usuario cadastrado
        if(firebaseAuth.getCurrentUser() != null){
            String email = firebaseAuth.getCurrentUser().getEmail();
            Log.i("USER", "Usuário logado: " + email);
        }
        else{
            Log.i("USER", "Usuário não logado.");
        }*/

        /*Cadastro de usuários*/
        firebaseAuth.createUserWithEmailAndPassword("otaviomiguel19@gmail.com", "101601")
                .addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Log.i("CreateUser", "Sucesso");
                        }else{
                            Log.i("CreateUser", "Fracasso");
                        }
                    }
                });



        /*
        DatabaseReference usuariosDB = databaseReference.child("usuarios");
        usuariosDB.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                TextView textView = findViewById(R.id.txtView);
                String firebase = dataSnapshot.getValue().toString();
                textView.setText(firebase);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });*/

        /*Usuarios usuarios = new Usuarios();
        usuarios.setNome("Otavio");
        usuarios.setSobrenome("Miguel");
        usuarios.setIdade(21);*/

        //usuariosDB.child("001").setValue(usuarios);

    }

    @Override
    protected void onStart() {
        if(firebaseAuth.getCurrentUser() != null){
            String email = firebaseAuth.getCurrentUser().getEmail();
            Log.i("USER", "Usuário logado: " + email + " " + firebaseAuth.getCurrentUser().getDisplayName());
        }
        else{
            Log.i("USER", "Usuário não logado.");
        }

        super.onStart();
    }
}
